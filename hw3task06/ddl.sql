create schema hw3;

create table hw3.employee_info
(
    id         integer primary key,
    name       varchar(50),
    city       varchar(50),
    department varchar(50),
    salary     integer
);

insert into hw3.employee_info
    (id, name, city, department, salary)
values (24, 'Марина', 'Москва', 'it', 104),
       (21, 'Елена', 'Самара', 'it', 84),
       (22, 'Ксения', 'Москва', 'it', 90),
       (25, 'Иван', 'Москва', 'it', 120),
       (23, 'Леонид', 'Самара', 'it', 104),
       (11, 'Дарья', 'Самара', 'hr', 70),
       (12, 'Борис', 'Самара', 'hr', 78),
       (31, 'Вероника', 'Москва', 'sales', 96),
       (33, 'Анна', 'Москва', 'sales', 100),
       (32, 'Григорий', 'Самара', 'sales', 96);
