import argparse

import pandas as pd
import psycopg2 as pg


def get_conn():
    pg.connect(
        """
        dbname='postgres'
        user='postgres'
        host='{creds.host}'
        port='5432'
        password='qwerty'
    """
    )


def main():
    parser = argparse.ArgumentParser(description="Check for the result")
    parser.add_argument("--solution", type=str, required=True, dest="solution_path")
    args = parser.parse_args()

    with open(args.solution_path, "r") as solution_file:
        file_content = solution_file.read()

    true_result = pd.read_csv("answer.csv")
    print(true_result)
    print(file_content)


if __name__ == "__main__":
    main()
