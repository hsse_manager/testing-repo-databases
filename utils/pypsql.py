import argparse
import os
import sys
import typing
from dataclasses import dataclass
from typing import NoReturn, Optional
from urllib.parse import quote

import pandas as pd
import psycopg2 as pg
import sqlalchemy


@dataclass
class Credentials:
    dbname: str = "postgres"
    host: str = "127.0.0.1"
    port: int = 5432
    user: str = "postgres"
    password: str = "password"


def _extract_credentials() -> Credentials:
    return Credentials(
        dbname=os.getenv("DBNAME", Credentials.dbname),
        host=os.getenv("DBHOST", Credentials.host),
        port=os.getenv("DBPORT", Credentials.port),
        user=os.getenv("DBUSER", Credentials.user),
        password=os.getenv("DBPASSWORD", Credentials.password),
    )


def psycopg2_conn_string(creds: Optional[Credentials] = None) -> str:
    if not creds:
        creds = _extract_credentials()
    return f"""
        dbname={creds.dbname!r}
        user={creds.user!r}
        host={creds.host!r}
        port={creds.port!r}
        password={creds.password!r}
    """


def psycopg2_conn(conn_string: Optional[str] = None):
    if not conn_string:
        conn_string = psycopg2_conn_string()
    return pg.connect(conn_string)


def psycopg2_execute_sql(sql: str, conn=None) -> NoReturn:
    if not conn:
        conn = psycopg2_conn()
    cursor = conn.cursor()
    cursor.execute(sql)
    conn.commit()


def sqlalchemy_conn_string(creds: Optional[Credentials] = None) -> str:
    if not creds:
        creds = _extract_credentials()
    return (
        "postgresql://"
        f"{creds.user}:{quote(creds.password)}@"
        f"{creds.host}:{creds.port}/{creds.dbname}"
    )


def sqlalchemy_conn(conn_string: Optional[str] = None):
    if not conn_string:
        conn_string = sqlalchemy_conn_string()
    return sqlalchemy.create_engine(conn_string)


def execute_sql_to_df(conn, sql: str) -> pd.DataFrame:
    return pd.read_sql(sqlalchemy.text(sql), con=conn)


def read_sql(filepath: str) -> str:
    with open(filepath, "r") as file:
        return file.read().rstrip()


def exec_sql(sql: str, verbose: bool) -> NoReturn:
    if verbose:
        sql = sql.replace("%", "%%")
        print(execute_sql_to_df(conn=sqlalchemy_conn(), sql=sql))
    else:
        psycopg2_execute_sql(sql=sql)


def exec_sql_file(path: str, cat: bool, verbose: bool) -> NoReturn:
    sql = read_sql(path)
    if cat:
        print(sql)
    exec_sql(sql, verbose)


def copy_csv(
    filepath: str,
    table: str,
    sep: str = ",",
    if_exists: str = "replace",
) -> typing.NoReturn:
    csv_df = pd.read_csv(filepath, delimiter=sep)

    schema = None
    if "." in table:
        schema, table = table.split(".")

    csv_df.to_sql(
        table, schema=schema, con=sqlalchemy_conn(), if_exists=if_exists, index=False
    )


def compare_pd_df(ground_truth: pd.DataFrame, solution: pd.DataFrame) -> typing.NoReturn:
    if (ground_truth.dtypes != solution.dtypes).any():
        sol_types = '\n'.join(str(solution.dtypes).split('\n')[:-1])
        true_types = '\n'.join(str(ground_truth.dtypes).split('\n')[:-1])
        print(
            f"""
                [WARNING] The answer has incorrect datatypes.
                You have
                {sol_types}
                dtypes instead of
                {true_types}
                dtypes."""
        )

    if set(ground_truth.columns) != set(solution.columns):
        sys.exit(
            f"""
        The answer has incorrect columns.
        True columns are: {list(ground_truth.columns)},\nyour columns are: {list(solution.columns)}"""
        )
    elif (ground_truth.columns != solution.columns).any():
        sys.exit(
            f"""
        The answer has incorrect columns order.
        True columns are: {list(ground_truth.columns)},\nyour columns are: {list(solution.columns)}"""
        )
    elif len(ground_truth.index) != len(solution.index):
        sys.exit(
            f"""
        The answer has incorrect number of rows.
        You have {len(solution.index)} row(s) instead of {len(ground_truth.index)} row(s)"""
        )
    elif (ground_truth.fillna('NULL') != solution.fillna('NULL')).any().any():
        different_idx = (ground_truth.fillna('NULL') != solution.fillna('NULL')).any(axis=1)
        sys.exit(
            f"""
        The answer is incorrect.
        You have:
        {solution[different_idx]}
        The result is:
        {ground_truth[different_idx]}"""
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--file", default="", dest="file", help="path to sql script to execute"
    )
    parser.add_argument(
        "--cat",
        default=False,
        dest="cat",
        action="store_true",
        help="output file content",
    )
    parser.add_argument("--sql", default="", dest="sql", help="sql script to execute")
    parser.add_argument(
        "--check", default="", dest="check", help="if should check the result"
    )
    parser.add_argument(
        "--table", default="", dest="table_name", help="table_name to insert data"
    )
    parser.add_argument(
        "--csv", default="", dest="csv", help="csv to insert into table_name"
    )
    parser.add_argument(
        "--verbose",
        default=False,
        dest="verbose",
        action="store_true",
        help="output execution result",
    )
    args = parser.parse_args()

    if args.file:
        exec_sql_file(args.file, args.cat, args.verbose)
        if args.check:
            true_result = pd.read_csv("answer.csv")

            with open(args.file, "r") as solution_file:
                solution_script = solution_file.read()
                solution_df = execute_sql_to_df(sqlalchemy_conn(), solution_script)

            compare_pd_df(true_result, solution_df)

    if args.sql:
        exec_sql(args.sql, args.verbose)

    if args.csv and args.table_name:
        copy_csv(args.csv, args.table_name)
