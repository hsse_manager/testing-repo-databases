#! /usr/bin/env bash

if [[ $1 == "master" ]]
then
    echo "No testing on master branch"
elif [[ $1 == "main" ]]
then
    echo "No testing on main branch"
else
    cp -R testing_repo/$1/. $1/
    echo "Copy utils"
    cp testing_repo/banned_words_checker.py $1/banned_words_checker.py
    cp testing_repo/result_checker.py $1/result_checker.py
    cp -R testing_repo/data/. $1/data/
    cp -R testing_repo/utils/. $1/utils/
    touch $1/__init__.py

    cd $1
    echo "begin testing $1"

    echo "Check for banned words"
    python3 banned_words_checker.py --solution=$1.sql --banned-words=banned_words.json
    if [[ ! $? -eq 0 ]]
    then
      echo "В Вашем коде есть запрещенные конструкции!"
      exit 1
    fi

    echo "Check for the result value"
    bash ddl.sh
    python3 utils/pypsql.py --file=$1.sql --check=1
    if [[ ! $? -eq 0 ]]
    then
      echo "Результат неверный!"
      exit 1
    fi
fi
